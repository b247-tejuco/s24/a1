//part1
const getCube= 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

//part2
const address= ['258', 'Washington Ave NW', 'California', '90011'];
const [streetNumber, streetAddress, country, stateZipCode]= address;
console.log(`I live at ${streetNumber} ${streetAddress}, ${country} ${stateZipCode}`);

//part3
const animal= {
	name: 'Lolong',
	specie: 'saltwater crocodile',
	weight: 1075,
	dimensions: {
		foot: 20,
		inches: 3
	}
}

//part4
const{ name, specie, weight, dimensions}= animal
console.log(`${name} was a ${specie}. He weighed at ${weight} kg with a measurement of ${dimensions.foot} ft ${dimensions.inches} in.`)

//part5
numberArr=[1, 2, 3, 4, 5];
numberArr.forEach((num)=> console.log(num));

//part6
let reducedArray= numberArr.reduce((num1, num2)=>{
	return num1 + num2
});
console.log(reducedArray);

//part7

class Dog {
	constructor(name, age, breed){
		this.name= name;
		this.age= age;
		this.breed= breed;
	}
}

const newDog= new Dog("Frankie", 5, "Miniature Dachshund")
console.log(newDog);
